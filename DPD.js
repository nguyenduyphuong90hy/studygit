var s_tc_DPD=new TagContainer('DPD');

function TagContainer(n){var t=this,w=t.w=window;t.d=w.document;t._c='s_t';if(!w.s_c_il){w.s_c_il=[];w.s_c_in=0}t._il=w.s_c_il;t._in=w.s_c_in;t._il[t._in]=t;w.s_c_in++;t.tcn=t.l=0;t.stc=function(n){
var t=this,l=t.w.s_c_il,i,x;t.tcn=n;if(l)for(i=0;i<l.length;i++){x=l[i];if(x&&x._c=='s_l'&&x.tagContainerName==n)t.l=x}};t.stc(n);t.xd=function(s){var t=this,x=0;if(
t.d.implementation&&t.d.implementation.createDocument)x=(new DOMParser).parseFromString(s,'text/xml');else if(t.w.ActiveXObject){x=new ActiveXObject('Microsoft.XMLDOM');x.async='false';x.loadXML(s)}
return x};t.xe=function(x,t){var a,b=[],i,j;for(i=0;i<2;i++){if(i>0)t=t.toLowerCase();a=x.getElementsByTagName(t);if(a)for(j=0;j<a.length;j++)b[b.length]=a[j]}return b};t.xt=function(x){var t=this,b=
"",l,i;l=x.childNodes;if(l)for(i=0;i<l.length;i++)b+=t.xt(l[i]);if(x.data)b+=x.data;return b};t.cp=function(x){var t=this,tn=Math.floor((new Date).getTime()/1000),ts=x.s,te=x.e,tp=1,l=t.d.location,h=
l.hostname,hm=x.h,hp=1,p=l.pathname,pm=x.p,pp=1,q=l.search,qm=x.q,qp=1,qi,qv,c=t.d.cookie,cm=x.c,cp=1,ci,cv,i;if(ts)tp=(tn>=ts&&(!te||tn<=te));if(hm){hp=0;if(h){i=0;while(!hp&&i<hm.length){if(
h.indexOf(hm[i])>=0)hp=1;i++}}}if(pm){pp=0;if(p){i=0;while(!pp&&i<pm.length){if(p.indexOf(pm[i])>=0)pp=1;i++}}}if(qm){qp=0;if(q){if(q.substring(0,1)=='?')q=q.substring(1);q='&'+q+'&';i=0;while(
!qp&&i<qm.length){qi=q.indexOf('&'+qm[i].k+'=');if(!qm[i].v&&qi<0)qi=q.indexOf('&'+qm[i].k+'&');if(qi>=0)if(qm[i].v){qv=q.substring(qi+qm[i].k.length+2);qi=qv.indexOf('&');if(qi>=0){qv=unescape(
qv.substring(0,qi));if(qv==qm[i].v)qp=1}}else qp=1;i++}}}if(cm){cp=0;if(c){c=';'+c+';';c=c.split('; ').join(';');i=0;while(!cp&&i<cm.length){ci=c.indexOf(';'+cm[i].k+'=');if(!cm[i].v&&ci<0)ci=
c.indexOf(';'+cm[i].k+';');if(ci>=0)if(cm[i].v){cv=c.substring(ci+cm[i].k.length+2);ci=cv.indexOf(';');if(ci>=0){cv=unescape(cv.substring(0,ci));if(cv==cm[i].v)cp=1}}else cp=1;i++}}}return(
tp&&hp&&pp&&qp&&cp)};t.cl=[];t.cn=t.cpn=0;t.crt=0;t.bl=[];t.crl=function(cn,cpn){var t=this;if(cn==t.cn&&cpn==t.cpn)t.cr()};t.cr=function(){var t=this,d=t.d,b,c,p,n=1,o,u,x,y,l,i;if(t.cl.length>0){if(
!d.body){if(!t.crt)t.crt=setTimeout(function(){t.crt=0;t.cr()},13)}else{b=d.body;while(n&&t.cn<t.cl.length){c=t.cl[t.cn];if(t.cdwb){u=t.cdwb;t.cdwb=0;u='<div>'+u.replace(/&/g,'&amp;').replace(
/<img /gi,'<IMG ').replace(/<\/img>/gi,'</IMG>').replace(/<script /gi,'<SCRIPT ').replace(/<script>/gi,'<SCRIPT>').replace(/<\/script>/gi,'</SCRIPT>').replace(/<iframe /gi,'<IFRAME ').replace(
/<\/iframe>/gi,'</IFRAME>')+'</div>';x=t.xd(u);l=t.xe(x,'IMG');for(i=0;i<l.length;i++){u=l[i].getAttribute('src');if(u)c.p.splice(t.cpn,0,{t:'i',u:u})}l=t.xe(x,'SCRIPT');for(i=0;i<l.length;i++){u=l[i]
.getAttribute('src');if(u)c.p.splice(t.cpn,0,{t:'s',u:u});else{u=t.xt(l[i]);if(u)c.p.splice(t.cpn,0,{t:'c',c:u})}}l=t.xe(x,'IFRAME');for(i=0;i<l.length;i++){u=l[i].getAttribute('src');if(u)c.p.splice(
t.cpn,0,{t:'f',u:u})}}if((t.cpn>0||!c.x||t.cp(c.x))&&c.p&&t.cpn<c.p.length){p=c.p[t.cpn];if(p.t=='b'&&p.u){u=p.u;o=new Image;t.bl[t.bl.length]=o;o.onload=function(){var i;for(i=0;i<t.bl.length;i++)if(
t.bl[i]&&t.bl[i].src==u){t.bl.splice(i,1);return}};o.src=u}if((p.t=='s'&&p.u)||(p.t=='c'&&p.c)){n=0;t.cpn++;u=p.u;o=d.createElement('script');o.type='text/javascript';o.setAttribute('async','async')
x='s_c_il['+t._in+']';y=x+'.crl('+t.cn+','+t.cpn+')';if(p.t=='s'){o.n=new Function(y);o.t=0;o.i=setInterval(function(){if(o.readyState=='loaded')o.t++;if(o.readyState=='complete'||o.t>2){o.c();o.n()}}
,50);o.c=function(){if(o.i){clearInterval(o.i);o.i=0}};o.onreadystatechange=function(){if(o.readyState=='complete'){o.c();o.n()}};o.onload=function(){o.c();o.n()};o.src=u}else o.text=x+'.cdw='+x+
'.d.write;'+x+'.cdwb="";'+x+'.d.write=function(m){'+x+'.cdwb+=m};'+"\n"+p.c+"\n"+x+'.d.write='+x+'.cdw;'+y;x=b;l=d.getElementsByTagName('HEAD');if(l&&l[0])x=l[0];if(x.firstChild)x.insertBefore(o,
x.firstChild);else x.appendChild(o)}if(p.t=='f'&&p.u){u=p.u;o=d.createElement('IFRAME');o.setAttribute('style','display:none');o.setAttribute('width','0');o.setAttribute('height','0');o.setAttribute(
'src',u);b.appendChild(o)}if(n)t.cpn++}else{t.cn++;t.cpn=0}}if(n&&t.l){for(x in t.l.wl)if(!Object.prototype[x]){u=t.w[x];x=t.l.wl[x];if(u&&x)for(i in x)if(!Object.prototype[i]){if(typeof(x[i])!=
'function'||(''+x[i]).indexOf('s_c_il')<0)u[i]=x[i]}}for(i=0;i<t.l.wq.length;i++){c=t.l.wq[i];u=c.f;if(u)if(c.o)x=t.w[c.o];else x=t.w;if(x[u]&&typeof(x[u])=='function'&&(''+x[u]).indexOf('s_c_il')<0){
if(c.a)x[u].apply(x,c.a);else x[u].apply(x)}}}}}};}

var s_tc_DPD=new TagContainer('DPD');

function TagContainer(n){var t=this,w=t.w=window;t.d=w.document;t._c='s_t';if(!w.s_c_il){w.s_c_il=[];w.s_c_in=0}t._il=w.s_c_il;t._in=w.s_c_in;t._il[t._in]=t;w.s_c_in++;t.tcn=t.l=0;t.stc=function(n){
var t=this,l=t.w.s_c_il,i,x;t.tcn=n;if(l)for(i=0;i<l.length;i++){x=l[i];if(x&&x._c=='s_l'&&x.tagContainerName==n)t.l=x}};t.stc(n);t.xd=function(s){var t=this,x=0;if(
t.d.implementation&&t.d.implementation.createDocument)x=(new DOMParser).parseFromString(s,'text/xml');else if(t.w.ActiveXObject){x=new ActiveXObject('Microsoft.XMLDOM');x.async='false';x.loadXML(s)}
return x};t.xe=function(x,t){var a,b=[],i,j;for(i=0;i<2;i++){if(i>0)t=t.toLowerCase();a=x.getElementsByTagName(t);if(a)for(j=0;j<a.length;j++)b[b.length]=a[j]}return b};t.xt=function(x){var t=this,b=
"",l,i;l=x.childNodes;if(l)for(i=0;i<l.length;i++)b+=t.xt(l[i]);if(x.data)b+=x.data;return b};t.cp=function(x){var t=this,tn=Math.floor((new Date).getTime()/1000),ts=x.s,te=x.e,tp=1,l=t.d.location,h=
l.hostname,hm=x.h,hp=1,p=l.pathname,pm=x.p,pp=1,q=l.search,qm=x.q,qp=1,qi,qv,c=t.d.cookie,cm=x.c,cp=1,ci,cv,i;if(ts)tp=(tn>=ts&&(!te||tn<=te));if(hm){hp=0;if(h){i=0;while(!hp&&i<hm.length){if(
h.indexOf(hm[i])>=0)hp=1;i++}}}if(pm){pp=0;if(p){i=0;while(!pp&&i<pm.length){if(p.indexOf(pm[i])>=0)pp=1;i++}}}if(qm){qp=0;if(q){if(q.substring(0,1)=='?')q=q.substring(1);q='&'+q+'&';i=0;while(
!qp&&i<qm.length){qi=q.indexOf('&'+qm[i].k+'=');if(!qm[i].v&&qi<0)qi=q.indexOf('&'+qm[i].k+'&');if(qi>=0)if(qm[i].v){qv=q.substring(qi+qm[i].k.length+2);qi=qv.indexOf('&');if(qi>=0){qv=unescape(
qv.substring(0,qi));if(qv==qm[i].v)qp=1}}else qp=1;i++}}}if(cm){cp=0;if(c){c=';'+c+';';c=c.split('; ').join(';');i=0;while(!cp&&i<cm.length){ci=c.indexOf(';'+cm[i].k+'=');if(!cm[i].v&&ci<0)ci=
c.indexOf(';'+cm[i].k+';');if(ci>=0)if(cm[i].v){cv=c.substring(ci+cm[i].k.length+2);ci=cv.indexOf(';');if(ci>=0){cv=unescape(cv.substring(0,ci));if(cv==cm[i].v)cp=1}}else cp=1;i++}}}return(
tp&&hp&&pp&&qp&&cp)};t.cl=[];t.cn=t.cpn=0;t.crt=0;t.bl=[];t.crl=function(cn,cpn){var t=this;if(cn==t.cn&&cpn==t.cpn)t.cr()};t.cr=function(){var t=this,d=t.d,b,c,p,n=1,o,u,x,y,l,i;if(t.cl.length>0){if(
!d.body){if(!t.crt)t.crt=setTimeout(function(){t.crt=0;t.cr()},13)}else{b=d.body;while(n&&t.cn<t.cl.length){c=t.cl[t.cn];if(t.cdwb){u=t.cdwb;t.cdwb=0;u='<div>'+u.replace(/&/g,'&amp;').replace(
/<img /gi,'<IMG ').replace(/<\/img>/gi,'</IMG>').replace(/<script /gi,'<SCRIPT ').replace(/<script>/gi,'<SCRIPT>').replace(/<\/script>/gi,'</SCRIPT>').replace(/<iframe /gi,'<IFRAME ').replace(
/<\/iframe>/gi,'</IFRAME>')+'</div>';x=t.xd(u);l=t.xe(x,'IMG');for(i=0;i<l.length;i++){u=l[i].getAttribute('src');if(u)c.p.splice(t.cpn,0,{t:'i',u:u})}l=t.xe(x,'SCRIPT');for(i=0;i<l.length;i++){u=l[i]
.getAttribute('src');if(u)c.p.splice(t.cpn,0,{t:'s',u:u});else{u=t.xt(l[i]);if(u)c.p.splice(t.cpn,0,{t:'c',c:u})}}l=t.xe(x,'IFRAME');for(i=0;i<l.length;i++){u=l[i].getAttribute('src');if(u)c.p.splice(
t.cpn,0,{t:'f',u:u})}}if((t.cpn>0||!c.x||t.cp(c.x))&&c.p&&t.cpn<c.p.length){p=c.p[t.cpn];if(p.t=='b'&&p.u){u=p.u;o=new Image;t.bl[t.bl.length]=o;o.onload=function(){var i;for(i=0;i<t.bl.length;i++)if(
t.bl[i]&&t.bl[i].src==u){t.bl.splice(i,1);return}};o.src=u}if((p.t=='s'&&p.u)||(p.t=='c'&&p.c)){n=0;t.cpn++;u=p.u;o=d.createElement('script');o.type='text/javascript';o.setAttribute('async','async')
x='s_c_il['+t._in+']';y=x+'.crl('+t.cn+','+t.cpn+')';if(p.t=='s'){o.n=new Function(y);o.t=0;o.i=setInterval(function(){if(o.readyState=='loaded')o.t++;if(o.readyState=='complete'||o.t>2){o.c();o.n()}}
,50);o.c=function(){if(o.i){clearInterval(o.i);o.i=0}};o.onreadystatechange=function(){if(o.readyState=='complete'){o.c();o.n()}};o.onload=function(){o.c();o.n()};o.src=u}else o.text=x+'.cdw='+x+
'.d.write;'+x+'.cdwb="";'+x+'.d.write=function(m){'+x+'.cdwb+=m};'+"\n"+p.c+"\n"+x+'.d.write='+x+'.cdw;'+y;x=b;l=d.getElementsByTagName('HEAD');if(l&&l[0])x=l[0];if(x.firstChild)x.insertBefore(o,
x.firstChild);else x.appendChild(o)}if(p.t=='f'&&p.u){u=p.u;o=d.createElement('IFRAME');o.setAttribute('style','display:none');o.setAttribute('width','0');o.setAttribute('height','0');o.setAttribute(
'src',u);b.appendChild(o)}if(n)t.cpn++}else{t.cn++;t.cpn=0}}if(n&&t.l){for(x in t.l.wl)if(!Object.prototype[x]){u=t.w[x];x=t.l.wl[x];if(u&&x)for(i in x)if(!Object.prototype[i]){if(typeof(x[i])!=
'function'||(''+x[i]).indexOf('s_c_il')<0)u[i]=x[i]}}for(i=0;i<t.l.wq.length;i++){c=t.l.wq[i];u=c.f;if(u)if(c.o)x=t.w[c.o];else x=t.w;if(x[u]&&typeof(x[u])=='function'&&(''+x[u]).indexOf('s_c_il')<0){
if(c.a)x[u].apply(x,c.a);else x[u].apply(x)}}}}}};}

function gup( name )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( document.getElementById("dbg").src );
  if( results == null )
    return true;
  else if( results[1] == "0" || results[1] == "false" )
    return false;
  else
    return true;
}
function s_rep(s,n,meth) {
        var sURL=s.replace(/\%3Cb\%3EParameter\:\%3C\/b\%3E\%20/gi,'').replace(/\%20\%3C\/td\%3E\%3Ctd\%3E\%20/gi,'');
		s = s.replace(/&([^&]*)/g,"~y~$1");
		
		if (s_object_names && meth.indexOf("Mbox") >= 0)
		{
//            s=s.replace(/(.*)(mbox=[^<]*)(<\/td><\/tr><tr><td align = .right. nowrap>)/,"$2</a></b>$3$1");

			s=s.replace(/(.*)~y~(mbox=[^<]*)/,"$2~y~$1");

			s=s.replace(/https*:\/\/([^\/]*omtrdc.net)[^?]*mbox\/([^?]*)\?/,"Request Domain</td><td> $1~y~Request Type </td><td> $2~y~ ");
			s=s.replace(/https*:\/\/([^\/]*omtrdc.net)[^?]*sc\/([^?]*)\?/,"Request Domain</td><td> $1~y~Request Type </td><td> $2~y~ ");

			s=s.replace(/mboxHost=/,"Mbox Host </td><td> ");
			s=s.replace(/mboxSession=/,"Mbox Session </td><td> ");
			s=s.replace(/mboxPC=/,"Mbox PC </td><td> ");
			s=s.replace(/mboxPage=/,"Mbox Page </td><td> ");
			s=s.replace(/mboxCount=/,"Mbox Count </td><td> ");
			s=s.replace(/mboxId=/,"Mbox Id </td><td> ");
			s=s.replace(/mboxTime=/,"Mbox Time </td><td> ");
			s=s.replace(/mboxURL=/,"Mbox URL </td><td> ");
			s=s.replace(/mboxReferrer=/,"Mbox Referrer </td><td> ");
			s=s.replace(/mboxVersion=/,"Mbox Version </td><td> ");
			s=s.replace(/mboxXDomain=/,"Third-Party Cookies </td><td> ");
			s=s.replace(/mbox=/,"Mbox Name</b></td><td> ");

			s=s.replace(/profile.([A-z0-9]+)=([^'"])/g,"<b>Profile Parameter:</b> $1 </td><td> $2");
			s=s.replace(/entity.([A-z0-9]+)=([^'"])/g,"<b>Entity Parameter:</b> $1 </td><td> $2");
			s=s.replace(/([A-z0-9]+)=([^'"])/g,"<b>Parameter:</b> $1 </td><td> $2");
            s=s.replace("Mbox Name</b></td><td>",'Mbox Name</b></td><td> <b><a href="'+sURL+'" title="'+sURL+'" target="_blank">');
			
//			s=s.replace(/,/g,", ");
			
			//s=s.substring(0,s.lastIndexOf("<tr>");
			if(n!=' ')
				s='<table class=\'debugtable\'><tr><td align=\'right\' width=\'165px\' nowrap><b>'+s+'</td></tr></table>'
		} 
		else if (s_object_names && (meth=="Survey" || meth=="Survey Rendered"))
		{
			s=s.replace(/.*\/survey\/dynamic\/[^\/]*\/[^\/]*\/([^\/]*)\/.*/,"Report Suite ID(s) </td><td> $1 </td></tr>");
			if (meth=="Survey Rendered")
			{
				s+="<tr><td align = 'right' nowrap> Rendered </td><td> true </td>";
				meth="Survey";
			}
				
			if(n!=' ')
s='<table class = \'debugtable\' ><tr><td align = \'right\' width=\'165px\' nowrap>'+s+'</td></tr></table>'
		}
 
		else if (s_object_names && meth=="Survey Result")
		{
			s=s.replace(/~g/," ");
			s=s.replace(/([A-z]+)=([^'"])/g," $1 </td><td> $2");
			if(n!=' ')
			s='<table  class = \'debugtable\' ><tr><th>Question</th><th>Answer</th></tr><tr><td align = \'right\' nowrap width=\"165px\">'+s+'</td></tr></table>'
		}
 
		else if (s_object_names && meth=="AudienceManager")
		{
			
			s=s.replace(/^https*:\/\/([^\/]*)\/event\?/,' Domain</td><td> <a href ~equal~ "~sURL~" target="_blank">$1</td></tr><tr><td align ~equal~ "right" nowrap> ');
			s=s.replace(/_ts=/,"Timestamp </td><td> ");
			s=s.replace(/d_fn=/,"First Name </td><td> ");
			s=s.replace(/d_zc=/,"Zip Code </td><td> ");
			s=s.replace(/d_zx=/,"Zip Code </td><td> ");
			s=s.replace(/d_dma=/,"DMA </td><td> ");
			s=s.replace(/d_ld=/,"Log Data </td><td> ");
			s=s.replace(/d_px=/g,"Actual trait IDs </td><td> ");
			s=s.replace(/d_cb=/,"Callback Wrapper</td><td> ");
			s=s.replace(/d_rtb=json/,"JSON Request </td><td> True");
			s=s.replace(/d_rtbd=json/,"JSON Request </td><td> True");
			s=s.replace(/d_dst=1/,"Return Destination </td><td> True");
			s=s.replace(/d_cts=1/,"Return traits and segments </td><td> True");
			
//			var context=s.replace(/.*~y~c\.([^\.]*)~y~\.c~y~.*/,"$1");
//			context = context.replace(/~y~/g,"~y~ <b>Context: </b>").replace(/=/g," </td><td> ");
//			s=s.replace(/~y~c\.([^\.]*)~y~\.c/,context);

			s=s.replace(/c_pageName=/,"Customer: pageName </td><td> ");
			s=s.replace(/c_pageType=/,"Customer: pageType </td><td> ");
			s=s.replace(/c_server=/,"Customer: server </td><td> ");
			s=s.replace(/c_products=/,"Customer: products </td><td name ~equal~ 'productList'> ");
			s=s.replace(/c_events=/,"Customer: events </td><td> ");
			s=s.replace(/c_xact=/,"Customer: Transaction ID </td><td> ");
			s=s.replace(/c_purchaseID=/,"Customer: purchase ID </td><td> ");
			s=s.replace(/c_campaign=/,"Customer: campaign </td><td> ");
			s=s.replace(/c_channel=/,"Customer: channel </td><td> ");
/*			s=s.replace(/c_pev1=/,"URL (non-page) </td><td> ");
			s=s.replace(/c_pev2=/,"Friendly Name (non-page) </td><td> ");
			s=s.replace(/c_pev3=(.*--\*\*--[^&]*)/,"Video Data </td><td> $1");
			s=s.replace(/c_pev3=/,"Survey Data </td><td> ");

			s=s.replace(/c_c([0-9]{1,3})=/g,"prop$1 </td><td> ");
			s=s.replace(/c_h([0-9]{1,3})=/g,"hier$1 </td><td> ");
			s=s.replace(/c_v([0-9]{1,3})=/g,"eVar$1 </td><td> ");
			s=s.replace(/c_l([0-9]{1,3})=/g,"list$1 </td><td> ");
			
			s=s.replace(/c_v0=/,"campaign </td><td> ");
			s=s.replace(/c_vmt=/,"Visitor Migration Key </td><td> ");
			s=s.replace(/c_vmf=/,"Visitor Migration Server </td><td> ");
			s=s.replace(/c_vvp=/,"Variable Provider (Genesis) </td><td> ");
			s=s.replace(/tnt=/,"TNT Var </td><td> ");
			s=s.replace(/c_state=/,"state </td><td> ");
			s=s.replace(/c_zip=/,"zip </td><td> ");
			s=s.replace(/c_vID=/,"Visitor ID </td><td> ");
			s=s.replace(/c_vid=/,"Visitor ID </td><td> ");
			s=s.replace(/c_pe=/,"Request Type </td><td> ");
			s=s.replace(/c_gn=/,"pageName </td><td> ");
			s=s.replace(/c_sv=/,"server </td><td> ");
			s=s.replace(/c_gt=/,"pageType </td><td> ");
			s=s.replace(/c_pl=/,"products </td><td> ");
			s=s.replace(/c_pi=/,"purchaseID </td><td> ");
			s=s.replace(/c_ev=/,"events </td><td> ");
			
			s=s.replace(/c_ce=/,"charSet </td><td> ");
			s=s.replace(/c_cc=/,"currencyCode </td><td> ");
			s=s.replace(/c_cdp=/,"cookieDomainPeriods </td><td> ");
			s=s.replace(/c_cl=/,"cookieLifetime </td><td> ");
			s=s.replace(/c_bw=/,"Browser Width </td><td> ");
			s=s.replace(/c_bh=/,"Browser Height </td><td> ");
			s=s.replace(/c_ct=/,"Connection Type </td><td> ");
			s=s.replace(/c_hp=/,"Home Page? </td><td> ");
			s=s.replace(/c_pid=/,"Page ID (ClickMap) </td><td> ");
			s=s.replace(/c_pidt=/,"Page ID Type (ClickMap) </td><td> ");
			s=s.replace(/c_ndh=/,"Sent From JavaScript File? </td><td> ");
			s=s.replace(/c_oid=/,"Object ID (ClickMap) </td><td> ");
			s=s.replace(/c_oidt=/,"Object ID Type (ClickMap) </td><td> ");
			s=s.replace(/c_ot=/,"Object Tag (ClickMap) </td><td> ");
			s=s.replace(/c_oi=/,"Source Index (ClickMap) </td><td> ");
			s=s.replace(/c_ns=/,"Name Space </td><td> ");
			s=s.replace(/c_ts=/,"Timestamp </td><td> ");
			s=s.replace(/c_AQB=/,"Query String Beginning </td><td> ");
			s=s.replace(/c_AQE=/,"Query String End </td><td> ");
			
			
			s=s.replace(/c_s=/,"Screen Resolution </td><td> ");
			s=s.replace(/c_c=/,"Color Depth </td><td> ");
			s=s.replace(/c_j=/,"JavaScript Version </td><td> ");
			s=s.replace(/c_v=/,"JavaScript Enabled </td><td> ");
			s=s.replace(/c_k=/,"Cookies Supported </td><td> ");
			s=s.replace(/c_r=/,"Referring URL </td><td> ");
			s=s.replace(/c_g=/,"Current URL </td><td> ");
			s=s.replace(/D=/,"Dynamic Copy of: ");
			s=s.replace(/c_p=/,"Plug-ins </td><td> ");
*/			s=s.replace(/c_/g,"Customer: ");
			s=s.replace(/d_/g,"System Variable: ");
			s=s.replace(/h_/g,"HTTP Header: ");
			s=s.replace(/p_/g,"PII: ");
			s=s.replace(/([a-z0-9])=([^'"~])/g,"$1 </td><td> $2");
			s=s.replace(/,/g,", ");
                    s=s.replace("~sURL~",sURL);
	
			
	
			
			if(n!=' ')
				s='<tr><td align = \'right\' width=\"165px\" nowrap>'+s.replace(/~equal~/g,"=")+'</td></tr></table>'
    } 
    else if (s_object_names && meth == "AdLens Image") 
    {
        var stats_domain = s.replace(/^https*:\/\/([^\/]*).*/, "$1").split('.');
        stats_domain = ((stats_domain[stats_domain.length - 2].length == 2 && stats_domain[stats_domain.length - 1].length == 2) ? stats_domain.slice(-3).join('.') : stats_domain.slice(-2).join('.'));
        var omtr_domain = stats_domain == "omtrdc.net" || stats_domain == "2o7.net";
        var cookie_type = window.location.host.indexOf(stats_domain) > -1 ? "First" : (omtr_domain ? "" : "Friendly ") + "Third";
        s += '~y~';
        s = s.replace(/^https*:\/\/([^\/]*)/, cookie_type + " Party Cookies</td><td> $1</td></tr><tr><td align = 'right' nowrap> ");
        s = s.replace(/nowrap> \/([0-9]{3,6})/gi, "nowrap> AdLens User ID </td><td> $1 </td></tr><tr><td align = 'right' nowrap> ");
        s = s.replace(/\/t\?/, "Pixel Type </td><td> Conversion </td></tr> ");
        s = s.replace(/\/p\?/, "Pixel Type </td><td> Page View </td></tr> ");
        s = s.replace(/\/s\?/, "Pixel Type </td><td> Segment </td></tr> ");
        s = s.replace(/ev_(\w*)=(\w*)(~y~|\z)/gi, "<tr><td align = 'right' nowrap> $1 </td><td> $2 </td></tr>");
        s = s.replace(/,/g, ", ");
        if (n != ' ')
            s = '<tr><td align = \'right\' width=\"165px\" nowrap>' + s + '</td></tr></table>'
		} 
		
		else if (s_object_names)
		{
			
		var stats_domain=s.replace(/^https*:\/\/([^\/]*).*/,"$1").split('.');
		stats_domain = ((stats_domain[stats_domain.length-2].length == 2&&stats_domain[stats_domain.length-1].length == 2)?stats_domain.slice(-3).join('.'):stats_domain.slice(-2).join('.'));
		var omtr_domain = stats_domain == "omtrdc.net" || stats_domain == "2o7.net";
			var cookie_type = window.location.host.indexOf(stats_domain) > -1 ? "First" : (omtr_domain?"":"Friendly ")+"Third";
			s=s.replace(/^https*:\/\/([^\/]*omtrdc.net)/,cookie_type+" Party Cookies</td><td> $1</td></tr><tr><td align = 'right' nowrap> ");
			s=s.replace(/^https*:\/\/([^\/]*2o7.net)/,cookie_type+" Party Cookies</td><td> $1</td></tr><tr><td align = 'right' nowrap> ");
			s=s.replace(/^https*:\/\/([^\/]*)/,cookie_type+" Party Cookies</td><td> $1</td></tr><tr><td align = 'right' nowrap> ");
			s=s.replace(/\/b\/ss\/([^\/]*)\/[^\/]*\/([^\/]*)\/[^?]*\?/,"Report Suite ID(s) </td><td> $1 </td></tr><tr><td align = 'right' nowrap> Version of Code </td><td> $2 </td></tr><tr><td align = 'right' nowrap>");
			s=s.replace(/\/b\/ss\/([^\/]*)\/[^?]*\?/,"Report Suite ID(s) </td><td> $1 </td></tr><tr><td align = 'right' nowrap> Version of Code </td><td> None </td></tr><tr><td align = 'right' nowrap>");
			s=s.replace(/v0=/,"campaign </td><td> ");
 
			s=s.replace(/pev1=/,"URL (non-page) </td><td> ");
			s=s.replace(/pev2=/,"Friendly Name (non-page) </td><td> ");
			s=s.replace(/pev3=(.*--\*\*--[^&]*)/,"Video Data </td><td> $1");
			s=s.replace(/pev3=/,"Survey Data </td><td> ");

			
			var subs=s.replace(/.*?(([cv][0-9]{1,2}=[^<]*<\/td><\/tr><tr><td align = .right. nowrap>)+).*/g,"$1");
			s=s.replace(/(.*?)((<\/td><\/tr><tr><td align = .right. nowrap>[cv][0-9]{1,2}=[^<]*)+)(.*)/g,"$1~~placeholder~~$4");
			//alert(s);
			subs=subs.replace(/([cv])([0-9])=/g,"$10$2=");
			subs=subs.replace(/c((\|){0,1}[0-9]{1,2})=/g,"~z~$1=");
			var subs_array = subs.split(/<\/td><\/tr><tr><td align = .right. nowrap>/g);
			subs_array.sort();
			subs=subs_array.join("<\/td><\/tr><tr><td align = \"right\" width=\"165px\" nowrap>");
			subs=subs.replace(/~z~/g,"c");
			subs=subs.replace(/([cv])0([0-9])/g,"$1$2");
			s=s.replace(/~~placeholder~~/,subs);
			
			s=s.replace(/c([0-9]{1,3})=/g,"prop$1 </td><td> ");
			s=s.replace(/h([0-9]{1,3})=/g,"hier$1 </td><td> ");
			s=s.replace(/v([0-9]{1,3})=/g,"eVar$1 </td><td> ");
			s=s.replace(/l([0-9]{1,3})=/g,"list$1 </td><td> ");
			
			var context=s.replace(/.*~y~c\.([^\.]*)~y~\.c~y~.*/,"$1");
			context = context.replace(/~y~/g,"~y~ <b>Context: </b>").replace(/=/g," </td><td> ");
			s=s.replace(/~y~c\.([^\.]*)~y~\.c/,context);

			s=s.replace(/pageName=/,"pageName </td><td> ");
			s=s.replace(/pageType=/,"pageType </td><td> ");
			s=s.replace(/server=/,"server </td><td> ");
			s=s.replace(/products=/,"products </td><td name='productList'> ");
			s=s.replace(/events=/,"events </td><td> ");
			s=s.replace(/xact=/,"Transaction ID </td><td> ");
			s=s.replace(/purchaseID=/,"purchase ID </td><td> ");
			s=s.replace(/ch=/,"channel </td><td> ");
			s=s.replace(/fid=/,"Visitor ID </td><td> ");
			s=s.replace(/vmt=/,"Visitor Migration Key </td><td> ");
			s=s.replace(/vmf=/,"Visitor Migration Server </td><td> ");
			s=s.replace(/vvp=/,"Variable Provider (Genesis) </td><td> ");
			s=s.replace(/tnt=/,"TNT Var </td><td> ");
			s=s.replace(/state=/,"state </td><td> ");
			s=s.replace(/zip=/,"zip </td><td> ");
			s=s.replace(/vID=/,"Visitor ID </td><td> ");
			s=s.replace(/vid=/,"Visitor ID </td><td> ");
			s=s.replace(/pe=/,"Request Type </td><td> ");
			/*Mobile */
			s=s.replace(/gn=/,"pageName </td><td> ");
			s=s.replace(/sv=/,"server </td><td> ");
			s=s.replace(/gt=/,"pageType </td><td> ");
			s=s.replace(/pl=/,"products </td><td> ");
			s=s.replace(/pi=/,"purchaseID </td><td> ");
			s=s.replace(/ev=/,"events </td><td> ");
			
			s=s.replace(/ce=/,"charSet </td><td> ");
			s=s.replace(/cc=/,"currencyCode </td><td> ");
			s=s.replace(/cdp=/,"cookieDomainPeriods </td><td> ");
			s=s.replace(/cl=/,"cookieLifetime </td><td> ");
			s=s.replace(/\/5\/=/,"mobile </td><td> ");
			s=s.replace(/\/1\/=/,"mobile </td><td> ");
			s=s.replace(/bw=/,"Browser Width </td><td> ");
			s=s.replace(/bh=/,"Browser Height </td><td> ");
			s=s.replace(/ct=/,"Connection Type </td><td> ");
			s=s.replace(/hp=/,"Home Page? </td><td> ");
			s=s.replace(/pid=/,"Page ID (ClickMap) </td><td> ");
			s=s.replace(/pidt=/,"Page ID Type (ClickMap) </td><td> ");
			s=s.replace(/ndh=/,"Sent From JavaScript File? </td><td> ");
			s=s.replace(/oid=/,"Object ID (ClickMap) </td><td> ");
			s=s.replace(/oidt=/,"Object ID Type (ClickMap) </td><td> ");
			s=s.replace(/ot=/,"Object Tag (ClickMap) </td><td> ");
			s=s.replace(/oi=/,"Source Index (ClickMap) </td><td> ");
			s=s.replace(/ns=/,"Name Space </td><td> ");
			s=s.replace(/ts=/,"Timestamp </td><td> ");
			s=s.replace(/AQB=/,"Query String Beginning </td><td> ");
			s=s.replace(/AQE=/,"Query String End </td><td> ");
			
			var current_date = s.replace(/.*~t=([^~]*).*/,"$1");
			current_date = formatDate(s_epa(current_date));
			s=s.replace(/t=([^~]*)/,"Date/Time </td><td> "+current_date);
			s=s.replace(/s=/,"Screen Resolution </td><td> ");
			s=s.replace(/c=/,"Color Depth </td><td> ");
			s=s.replace(/j=/,"JavaScript Version </td><td> ");
			s=s.replace(/v=/,"JavaScript Enabled </td><td> ");
			s=s.replace(/k=/,"Cookies Supported </td><td> ");
			s=s.replace(/r=/,"Referring URL </td><td> ");
			s=s.replace(/g=/,"Current URL </td><td> ");
			s=s.replace(/D=/,"Dynamic Copy of: ");
			s=s.replace(/p=/,"Plug-ins </td><td> ");
//			s=s.replace(/=/," </td><td> ");
			s=s.replace(/,/g,", ");
        
			
	
			
			if(n!=' ')
				s='<table class = \'debugtable\' ><tr><td align = \'right\' width=\"165px\" nowrap>'+s+'</td></tr></table>'
		} 
		
		if(url_decode) s=s_epa(s);	
		s = s.replace(/~y~/g,n);
		
		return s
}
 
function splitProducts(prodString)
	{
	resultsContent = "";
	productArray = new Array();
//split on comma into products

//split each product into base parts (category, Sku, Quantity, Total Cost, Events, eVars)
tempProductArray = prodString.split(",");
for(var i=0;i<tempProductArray.length;i++)
	{
	productArray[i] = tempProductArray[i].split(";");
	}
resultsContent=resultsContent+"<table class='producttable'><tr><th>Category</th><th>Product ID</th><th>Qty</th><th>Total Cost</th><th>Events</th><th>eVars</th></tr>";
for(var j=0;j<productArray.length;j++)
	{
	resultsContent=resultsContent+"<tr>";
	
	for(var k=0;k<6;k++)
		{
		if(productArray[j][k])
			{
			resultsContent=resultsContent+"<td style='vertical-align:top;'>";
			if (k<4)
				{
				resultsContent=resultsContent+productArray[j][k];
				}
			else
				{
				eventArray=productArray[j][k].split("|");
				for(var l=0;l<eventArray.length;l++)
					{
					if (!l==0)resultsContent=resultsContent+"<br />";
					resultsContent=resultsContent+eventArray[l];
					}
				}
			resultsContent=resultsContent+" </td>";
			}
		else
			{
			resultsContent=resultsContent+"<td style='vertical-align:top;'> </td>";
			}
		}
	resultsContent=resultsContent+"</tr>";
	}
	
	resultsContent=resultsContent+"</table>";
	
	//document.getElementById("prodResults").innerHTML=resultsContent;
	return resultsContent;
	}

function s_epa(s) {
//	return unescape(s.replace(/\+/g,' '))
//alert(s);
//alert(decodeURIComponent(s));
	var ret_val = "";
	if(/\%u/.test(s))
		ret_val = unescape(s.replace(/\+/g,' '));
	else
		try{ret_val = decodeURIComponent(s);}
		catch (e) { ret_val = unescape(s.replace(/\+/g,' '));}
	
	return ret_val;
}
 
function formatDate(current_date) {


var month_names = new Array ( );
month_names[month_names.length] = "January";
month_names[month_names.length] = "February";
month_names[month_names.length] = "March";
month_names[month_names.length] = "April";
month_names[month_names.length] = "May";
month_names[month_names.length] = "June";
month_names[month_names.length] = "July";
month_names[month_names.length] = "August";
month_names[month_names.length] = "September";
month_names[month_names.length] = "October";
month_names[month_names.length] = "November";
month_names[month_names.length] = "December";
var hour = parseInt(current_date.replace(/([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4,4}) ([0-9]{1,2}):([0-9]{1,2}).*/,"$4"));
var minute = parseInt(current_date.replace(/([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4,4}) ([0-9]{1,2}):([0-9]{1,2}).*/,"$5"));

return month_names[parseInt(current_date.replace(/([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4,4}) ([0-9]{1,2}):([0-9]{1,2}).*/,"$2"))] + " " + current_date.replace(/([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4,4}) ([0-9]{1,2}):([0-9]{1,2}).*/,"$1") + ", " + current_date.replace(/([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4,4}) ([0-9]{1,2}):([0-9]{1,2}).*/,"$3") + " at " + (hour>12?hour-12:hour) + ":" + (minute<10?"0"+minute:minute) + " " + (hour>12?"PM":"AM");

}
// Requests in Images, Flash Movies, and global/window member image objects
function request_list_get(sc, tnt, recs, am, survey, amo) {
    var 
    request_list = new Array;
    document.getElementById("sc_count").innerHTML = "0";
    document.getElementById("tnt_count").innerHTML = "0";
    document.getElementById("recs_count").innerHTML = "0";
    document.getElementById("survey_count").innerHTML = "0";
    document.getElementById("am_count").innerHTML = "0";
    document.getElementById("amo_count").innerHTML = "0";
    
	if (window.opener) {
		// Recommendations Script
		if (window.opener.document.getElementsByTagName("script")) {
			scriptList = window.opener.document.getElementsByTagName("script");
			for (var image_num = 0;image_num < scriptList.length;image_num++) {
				var
					src = scriptList[image_num].src;
				if (src && src.indexOf('mboxSession') >= 0 && src.indexOf('entity') >= 0) {
					if(recs){
						var request = new Object;
						request.code = 'recs';
						request.method = 'Recommendations Mbox';
						request.url    = src;
						request_list[request_list.length] = request;
					}
					document.getElementById("recs_count").innerHTML++;
		
				}
			}
		}
		

		// TNT Script
		if (window.opener.document.getElementsByTagName("script")) {
			scriptList = window.opener.document.getElementsByTagName("script");
			for (var image_num = 0;image_num < scriptList.length;image_num++) {
				var
					src = scriptList[image_num].src;
				if (src && src.indexOf('mboxSession') >= 0 && src.indexOf('entity') < 0 ) {
					if(tnt){
						var request = new Object;
						request.code = 'tnt';
						request.method = src.indexOf('cookiesEnabled') >= 0 ? 'SiteCatalyst > Test&Target Integration Mbox' :'Test&Target Mbox';
						request.url    = src;
						request_list[request_list.length] = request;
					}
					document.getElementById("tnt_count").innerHTML++;
		
				}
				
			}
		}
		

		// AudienceManager Script
		if (window.opener.document.getElementsByTagName("script")) {
			scriptList = window.opener.document.getElementsByTagName("script");
			for (var image_num = 0;image_num < scriptList.length;image_num++) {
				var
					src = scriptList[image_num].src;
				if (src && src.indexOf('demdex.net/event') >= 0 || src.indexOf('dpm.demdex.net') >= 0 ) {
					if(tnt){
						var request = new Object;
						request.code = 'am';
						request.method = 'AudienceManager';
						request.url    = src;
						request_list[request_list.length] = request;
					}
					document.getElementById("am_count").innerHTML++;
		
				}
				
			}
		}
		

		// Survey
			
		if (window.opener.document.getElementsByTagName("script")) {
			scriptList = window.opener.document.getElementsByTagName("script");
			for (var image_num = 0;image_num < scriptList.length;image_num++) {
				var
					src = scriptList[image_num].src;
//				alert (src);
				if (src && src.indexOf('list.js') >= 0 && (/survey.1.2.2o7.net/i.test(src) || /sv.d..omtrdc.net/i.test(src) )) {
					if(survey){
						var request = new Object;
						request.code = 'survey';
						request.method = 'Survey';
						request.url    = src;
						request_list[request_list.length] = request;
					}
					document.getElementById("survey_count").innerHTML++;
		
				}
				if (request && src && src.indexOf('render.js') >= 0 && (/survey.1.2.2o7.net/i.test(src) || /sv.d..omtrdc.net/i.test(src) )) {
					
					request_list[request_list.length-1].method = 'Survey Rendered';

				}
			}
		}
		if (window.opener.document.images) {
			for (var image_num = 0;image_num < window.opener.document.images.length;image_num++) {
				var
					src = window.opener.document.images[image_num].src;
				if (src && /.net.survey.gather/i.test(src)) {
					var
						request = new Object;
					request.code = 'survey';
					request.method = 'Survey Response';
					request.url    = src;
					request_list[request_list.length] = request;
				}
			}
		}
		
		// SiteCatalyst Images
		if (window.opener.document.images) {
			for (var image_num = 0;image_num < window.opener.document.images.length;image_num++) {
				var
					src = window.opener.document.images[image_num].src;
				if (src.indexOf('/b/ss/') >= 0) {
					if(sc){
						var request = new Object;
						request.code = 'sc';
						request.method = 'SiteCatalyst Image';
						request.url    = src;
						request_list[request_list.length] = request;
					}
					document.getElementById("sc_count").innerHTML++;
		
				}
			}
		}
	// AdLens Images
        if (window.opener.document.images) {
            for (var image_num = 0; image_num < window.opener.document.images.length; image_num++) {
                var 
                src = window.opener.document.images[image_num].src;
                if (src && src.indexOf('everest') >= 0) {
                    if (amo) {
                        var request = new Object;
                        request.code = 'amo';
                        request.method = 'AdLens Image';
                        request.url = src;
						request_list[request_list.length] = request;
					}
					document.getElementById("amo_count").innerHTML++;
		
				}
			}
		}
		// Global Image Objects
		for (var window_member in window.opener) {
			if ((window_member.substring(0,4) == 's_i_') && (window.opener[window_member].src)) {
				var
					src = window.opener[window_member].src;
				if (src.indexOf('/b/ss/') >= 0) {
					if(sc){
						var request = new Object;
						request.code = 'sc';
						request.method = 'SiteCatalyst Image';
						request.url    = src;
						request_list[request_list.length] = request;
					}
					document.getElementById("sc_count").innerHTML++;
		
				}
			}
		}
		// Flash Movies
		var
			movie_list = new Array;
		function get_movies(obj) {
			if (obj) {
				try {
					if ((obj.tagName) &&
					    ((obj.tagName.toUpperCase() == 'OBJECT') ||
					     (obj.tagName.toUpperCase() == 'EMBED'))) {
						obj.GetVariable('_root'); // Call to throw and error if this is not a movie
						movie_list[movie_list.length] = obj;
					}
				} catch(e) {}
				obj = obj.firstChild;
				while (obj) {
					get_movies(obj);
					obj = obj.nextSibling;
				}
			}
		}
		if (window.opener.document) {
			get_movies(window.opener.document.body);
			for (var movie_num = 0;movie_num < movie_list.length;movie_num++) {
				try {
					var
						movie = movie_list[movie_num],
						movie_request_list_str = movie.GetVariable("_root.s_s.requestList"),
						movie_request_list = (movie_request_list_str ? movie_request_list_str.split(',h') : 0);
					if (movie_request_list) {
						for (var movie_request_num = 0;movie_request_num < movie_request_list.length;movie_request_num++) {
							if(sc){
								var request = new Object;
								request.method = 'Flash';
								request.code = 'flash';
								request.url    = (movie_request_list[movie_request_num].indexOf('http')==-1?'h':'')+movie_request_list[movie_request_num];
								request_list[request_list.length] = request;
							}
							document.getElementById("sc_count").innerHTML++;
						}
					}
				} catch(e) {}
			}
		}
	}
 
	return request_list;
}
 
function request_list_display(request_list,auto_refresh,url_decode,s_object_names) {
	var
		cell = document.getElementById('request_list_cell'),
		display = '';
	if (cell) {
		if ((auto_refresh) || (cell.innerHTML.toUpperCase().indexOf("TABLE") < 0)) {
			display += "<table border=\"0\" cellpadding=\"2\" cellspacing=\"1\" width=\"100%\"><tr><td style=\"font:bold 11px arial,sans-serif;color:#fff;padding: 1px 3px 1px 3px; \">"+request_list.length+" Request"+(request_list.length==1?"":"s")+" Displayed</td></tr>";
			if (request_list.length > 0) {
				for (var request_num = 0;request_num < request_list.length;request_num++) {
					var request_box = s_rep(request_list[request_num].url,(s_object_names? "</td></tr><tr><td width=\"165px\" align = \'right\' nowrap>" :"<br>"),request_list[request_num].method);

					display += ''
						+ "<tr bgcolor=\"" + (request_num % 2 == 0 ? "#EFEFEF" : "#EFEFEF") + "\"><td style=\"border: 0 none;font:11px arial,sans-serif;color:#000000;\">"
						+ "<table class=\'debugtable\'><tr><td align=\'left\' width=\'165px\' nowrap colspan=\"2\"><span style=\"font:bold 11px arial,sans-serif;color:#000000;padding-left:5px;\"><a style=\"color:#444;\"name=\""+request_list[request_num].code+"\">#"  + (request_num+1) + "</a> - " + request_list[request_num].method + "</span> " + (request_list[request_num].url.length>2075?"<span style=\"padding:4px 0 0 0;font:bold 11px arial,sans-serif;color:#FF0000;\">"+ request_list[request_num].url.length + " chars" + "</span><a onMouseOver=\"javascript:document.getElementById('char_limit').style.visibility='visible'\" onMouseOut=\"javascript:document.getElementById('char_limit').style.visibility='hidden'\"><img src='http://www.omniture-static.com/images/icons/mini-help-icon.gif' /></a>" : request_list[request_num].url.length + " chars") + "</td></tr>"
					//	+ (url_decode ? s_epa(s_rep(request_list[request_num].url,"&","<br>",request_list[request_num].method)) : s_rep(request_list[request_num].url,"&","<br>",request_list[request_num].method))
						+ request_box + "</td></tr>"
						+ "<tr style=\"font-size:8px;\"><td height=\"8px\"> </td></tr>"
						
					;
				}
			} else {
				display += "<tr><td align=\"center\" style=\"font:11px arial,sans-serif;color:#FF0000;\"><br />No Requests Found<br /><br /></td></tr>";
			}
			display += "</table>";
 
			cell.innerHTML = display;
			if (document.getElementsByName('productList'))
				for(var i=0; i < document.getElementsByName('productList').length; i++)
					document.getElementsByName('productList')[i].innerHTML=splitProducts(document.getElementsByName('productList')[i].innerHTML);

		}
	}
}
 
function request_list_run() {
	var qs = "";
		if(!window.auto_refresh) qs+="auto_refresh=0&";
		if(!window.url_decode) qs+="url_decode=0&";
		if(!window.s_object_names) qs+="s_object_names=0&";
		if(!window.sc) qs+="sc=0&";
		if(!window.tnt) qs+="tnt=0&";
		if(!window.recs) qs+="recs=0&";
		if(!window.am) qs+="am=0&";
		if(!window.survey) qs+="survey=0&";
		if (!window.amo) qs += "amo=0&";
	document.getElementById("bookmarklet").href="javascript:void(window.open(%22%22,%22stats_debugger%22,%22width=600,height=600,location=0,menubar=0,status=1,toolbar=0,resizable=1,scrollbars=1%22).document.write(%22<script%20language=\\\%22JavaScript\\\%22%20id=dbg%20src=\\\%22https://www.adobetag.com/d1/digitalpulsedebugger/live/DPD.js?"+qs+"\\\%22></%22+%22script>%22%20+%20%22<script%20language=\\\%22JavaScript\\\%22>window.focus();</script>%22));";
	
	request_list_display(
		request_list_get(window.sc,window.tnt,
		window.recs,window.am,window.survey,window.amo),
		window.auto_refresh,
		window.url_decode, 
		window.s_object_names
		
	);
	setTimeout("request_list_run()",5000);
}
 
	//Header
	document.write("<html><head><title>Adobe Marketing Cloud Debugger</title>"
	//+ "<link rel=\"shortcut icon\" href=\"http://www.digitalpulse.omniture.com/dp/digitalpulse.ico\">"
	//+ "<link rel=\"stylesheet\" type=\"text/css\" href=\"http://www.digitalpulse.omniture.com/dp/debugger.css\">"
	+ "<style type=\"text/css\">body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, form, fieldset, input, textarea, p, blockquote, th, td{margin:0;padding:0;border-color:#999}body{padding:0px;background-color:#4d4d4d;}td,th{padding:0px;font-size:8pt;text-decoration:none;border-bottom:0px solid #999;}.producttable{border:0 none;border-collapse:collapse;font:11px arial,sans-serif;color:#000;padding:0px;text-align:left;width:100%;} .producttable th{background-color:#CED8D0;border:1px solid #AAA;padding:1px 3px 1px 3px}.debugtable{border:0 none;border-collapse:collapse;font:11px arial,sans-serif;color:rgb(0, 0, 0);padding:0px;text-align:left;width:100%;}.debugtable td,th{border:1px solid #AAA;padding:1px 3px 1px 3px}.options tr{height:28px;}.bookmarklet{-moz-background-clip:border;-moz-background-inline-policy:continuous;-moz-background-origin:padding;background:#70A100 none repeat scroll 0 0;border:2px outset #70A100;color:#FFFFFF;text-decoration:none;}div.help{color:#293431;padding:4px 4px 4px 8px;visibility:hidden;z-index:3;border:1px solid #000;background-color:#CED8D0;position:absolute;width:500;top:50;font-weight:bold;margin-left:25px;}.options{border:0px;font-weight:normal;color:#fff;}.options .optionslabel{font:11px arial,sans-serif;color:#fff;margin-left:3px;} .options input{position:relative;top:2px;background-color:#fff;}p.rpt_title{color:#293431;margin:3px 4px 4px 16px;background-color:#ffffff;}h1.rpt_title{font-family: AdobeClean,adobe-clean,sans-serif;color:#f0f0f0;float:left;font-size:14px;font-weight:400;webkit-font-smoothing: antialiased;margin-top:6px;}.help_link{float:right;color:#ffffff;}.help_link a{color:#ffffff;}.rpt_header{-moz-background-clip:border;-moz-background-inline-policy:continuous;-moz-background-origin:padding;background:#222 scroll left bottom;width:100%;height:20px;padding:4px 0px 8px;margin-top:10px;}a{color:#ccc;}.debugtable a{color:#444;}img{border-width:0px;}table{border-spacing:0;}body, input, textarea, select, .text{color:#293431;font-family:arial,'lucida console',sans-serif;font-size:8pt;}.logo {background: url(\"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iaWNvbiIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMCIgeT0iMCIJIHdpZHRoPSIyNHB4IiBoZWlnaHQ9IjI0cHgiIHZpZXdCb3g9IjAgMCAyNCAyNCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMjQgMjQiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBmaWxsPSJub25lIiBzdHJva2U9IiNGRkZGRkYiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBkPSIJTTguMzEsMTEuODNjMCwyLjA0LDEuNjUsMy42ODksMy42OSwzLjY4OXMzLjY4OS0xLjY0OSwzLjY4OS0zLjY4OWMwLTIuMDUtMS42NDktMy43LTMuNjg5LTMuN1M4LjMxLDkuNzgsOC4zMSwxMS44M3ogTTQuOTIsMTEuODMJYzAsMy45MSwzLjE3LDcuMDgsNy4wOCw3LjA4czcuMDgtMy4xNyw3LjA4LTcuMDhjMC0zLjkyLTMuMTctNy4wOS03LjA4LTcuMDlTNC45Miw3LjkxLDQuOTIsMTEuODN6IE0yLDEyLjAyCWMwLDUuNTMsNC40OCwxMC4wMSwxMCwxMC4wMWM1LjUyLDAsMTAtNC40NzksMTAtMTAuMDFDMjIsNi40OCwxNy41MiwyLDEyLDJDNi40OCwyLDIsNi40OCwyLDEyLjAyeiIvPjxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBmaWxsPSIjRkZGRkZGIiBkPSJNMTQuNzIxLDIyLjYyYzIuODUtMC43NjEsNS4yMTktMi41OTEsNi42ODktNS4xNGwwLjE3LTAuMzAxCWwtMS43Ni0xLjAybC0wLjE3LDAuM2MtMS4yLDIuMDgtMy4xNDEsMy41Ni01LjQ2MSw0LjE5Yy0wLjc2LDAuMTk5LTEuNTQ5LDAuMzEtMi4zMywwLjMxYy0xLjU2LDAtMy4xMDktMC40Mi00LjQ3OS0xLjIxCWMtMS4yNS0wLjczLTIuMjktMS43MS0zLjA2LTIuOTJsLTAuMTktMC4yOWwtMS43MSwxLjExTDIuNiwxNy45M2MwLjk0LDEuNDcxLDIuMjQsMi43MTEsMy43NiwzLjU5QzguMDQsMjIuNDksOS45NCwyMywxMS44NSwyMwlDMTIuODIsMjMsMTMuNzc5LDIyLjg3LDE0LjcyMSwyMi42MnogTTE4Ljg4LDE1LjgzbDAuMTYtMC4zMDFjMC42MDEtMS4xNDksMC45Mi0yLjQ0OSwwLjkyLTMuNzU5YzAtMi4xNy0wLjg0LTQuMi0yLjM3LTUuNzQJYy0xLjUyOS0xLjUzLTMuMDktMi4zNy01LjI1LTIuMzdIMTJWNS43aDAuMzRjMS42MiwwLDIuNjYsMC42MywzLjgxMSwxLjc4YzEuMTQsMS4xNCwxLjc3LDIuNjcsMS43Nyw0LjI5CWMwLDAuOTgtMC4yNCwxLjk1MS0wLjY4OSwyLjgxbC0wLjE2LDAuM0wxOC44OCwxNS44M3ogTTcuNjEsMTMuOGMtMC4zMS0wLjY0LTAuNDYtMS4zMTktMC40Ni0yLjAzYzAtMS4yNiwwLjQ5LTIuNDUsMS4zOC0zLjM0CWMwLjg5LTAuODksMS44Ny0xLjM4LDMuMTMtMS4zOEgxMnYyLjA0aC0wLjM0Yy0wLjcxLDAtMS4xOCwwLjI4LTEuNjksMC43OGMtMC41LDAuNTEtMC43OCwxLjE4LTAuNzgsMS45CWMwLDAuNCwwLjA5LDAuNzkxLDAuMjYsMS4xNWwwLjE1LDAuMzAxTDcuNzUsMTQuMUw3LjYxLDEzLjh6Ii8+PC9zdmc+\") no-repeat scroll 0 0 / 24px 24px rgba(0, 0, 0, 0); float:left; height: 24px;width: 24px; margin-left: 8px;margin-right:6px; margin-top: 2px;min-width: 24px;}</style>"
	+ "</head><body style=\"padding:0px\">"
	+ "<form name=\"debugger\">"
	+ "<div id=\"help\" class=\"help\">The Marketing Cloud Debugger will refresh the request list every 5 seconds in case another event occurs. You can disable the auto refresh below. This tool will not work with sites that use frames. <br/><br/>To save the options you have selected, you can drag and drop the bookmark link to your bookmark toolbar. You can then delete the old version of the bookmark.</div>"
	+ "<div id=\"char_limit\" class=\"help\">There is a 2083 character limit to the length of a URL for IE versions 4-8. IE9 extends the length to over 5000 characters. This limit can cause truncation in your data collection for applicable users.  The Marketing Cloud Debugger alerts you to situations where you may need to reduce the length of the URL to ensure quality data capture.</div>"
	+ "<div style=\"height:16px;color:#fff;float:left;TEXT-ALIGN: right;width:100%;padding-top:6px;padding-bottom:5px;background-color:#3c3c3c;\"><span style=\"float:left;padding-top:0px;padding-left:10px\"><a onMouseOver=\"javascript:document.getElementById('help').style.visibility='visible'\" onMouseOut=\"javascript:document.getElementById('help').style.visibility='hidden'\" style='color:white'>Help</a></span>Bookmark Current Options: <a style=\"BORDER-BOTTOM: #000 2px outset; BORDER-LEFT: #000 2px outset; BACKGROUND: #000; COLOR: #fff; BORDER-TOP: #000 2px outset; BORDER-RIGHT: #000 2px outset; TEXT-DECORATION: none\" id=\"bookmarklet\" href=\"javascript:void(window.open(%22%22,%22stats_debugger%22,%22width=600,height=600,location=0,menubar=0,status=1,toolbar=0,resizable=1,scrollbars=1%22).document.write(%22<script%20language=\\\%22JavaScript\\\%22%20id=dbg%20src=\\\%22https://www.adobetag.com/d1/digitalpulsedebugger/live/DPD.js\\\%22></%22+%22script>%22%20+%20%22<script%20language=\\\%22JavaScript\\\%22>window.focus();</script>%22));\">Adobe Debugger</a></div></br />"
	+ "<div style=\"height:28px;color:#ff0a29;float:left;TEXT-ALIGN: center;width:100%;padding-top:6px;padding-bottom:5px;background-color:#FFF;text-transform: uppercase;font-size:12px;\"><span style=\"font-weight:bold;\">NOTICE:</span>This Debugging Tool has been Deprecated.  Please use the supported Adobe Experience Cloud Debugger Chrome Extension <a style=\"color: #1e1e1e;\" href=\"http://bit.ly/2LtoBdl\" target=\"_blank\">http://bit.ly/2LtoBdl</a></div>"
	+ "<div class=\"rpt_header\"><div class=\"logo\"></div><h1 class=\"rpt_title\">Adobe Marketing Cloud Debugger</h1><span id=\"box\"></span></div>"
//	+ "<p class=\"rpt_title\"><a href=\"javascript:void(0);\" onclick=\"window.close();\">Close window</a></p>"
);


	 
	// Requests 
	document.write(''
		+ "<table border=\"0\" width=\"100%\">"
		+ "<tr height=\"28px\"><td width=\"5%\" ><table border=\"0\" width=\"575\"><tr><td class=\"options\">"
		+ "<input type=\"checkbox\" name=\"box_sc\" onclick=\"window.sc=this.checked;request_list_run()\" "+(gup("sc") ? "CHECKED" : "")+"> "
		+ "<a href=\"http://www.omniture.com/en/products/online_analytics/sitecatalyst?s_cid=dp_debugger_sc\" target=\"_blank\"> SiteCatalyst</a> <b>[<span id=\"sc_count\">0</span>]</b></td><td class=\"options\">"
		+ "<input type=\"checkbox\" name=\"_box_tnt\" onclick=\"window.tnt=this.checked;request_list_run()\" "+(gup("tnt") ? "CHECKED" : "")+">"
		+ "<a href=\"http://www.omniture.com/en/products/conversion/testandtarget?s_cid=dp_debugger_tnt\" target=\"_blank\"> Test&Target</a> <b>[<span id=\"tnt_count\">0</span>]</b></td><td class=\"options\">"
		+ "<input type=\"checkbox\" name=\"box_recs\" onclick=\"window.recs=this.checked;request_list_run()\" "+(gup("recs") ? "CHECKED" : "")+">"
		+ "<a href=\"http://www.omniture.com/en/products/conversion/recommendations?s_cid=dp_debugger_recs\" target=\"_blank\"> Recommendations</a> <b>[<span id=\"recs_count\">0</span>]</b></td><td class=\"options\">"
		+ "<input type=\"checkbox\" name=\"box_am\" onclick=\"window.am=this.checked;request_list_run()\" "+(gup("am") ? "CHECKED" : "")+">"
		+ "<a href=\"http://www.omniture.com/en/products/advertising/audience-manager?s_cid=dp_debugger_am\" target=\"_blank\"> AudienceManager</a> <b>[<span id=\"am_count\">0</span>]</b></td><td class=\"options\">"
		+ "<input type=\"checkbox\" name=\"box_surv\" onclick=\"window.survey=this.checked;request_list_run()\" "+(gup("survey") ? "CHECKED" : "")+">"
		+ "<a href=\"http://www.omniture.com/en/products/conversion/survey?s_cid=dp_debugger_surv\" target=\"_blank\"> Survey</a> <b>[<span id=\"survey_count\">0</span>]</b>"
		+ "<input type=\"checkbox\" name=\"box_amo\" onclick=\"window.amo=this.checked;request_list_run()\" " + (gup("amo") ? "CHECKED" : "") + "> " 
		+ "<a href=\"http://www.adobe.com/solutions/media-optimization.html?s_cid=dp_debugger_amo\" target=\"_blank\"> AdLens</a> <b>[<span id=\"amo_count\">0</span>]</b></td><td class=\"options\">" 
		+ "</td></tr></table></td></tr>"
		+ "<tr height=\"28px\"><td class=\"options\" style=\"border-bottom:1px solid #999999\"><b>Options: </b>"
		+ "<input type=\"checkbox\" name=\"url_decode\" onclick=\"window.url_decode=this.checked;request_list_run()\" "+(gup("url_decode") ? "CHECKED" : "")+">"
		+ "<span style=\"font:11px arial,sans-serif;\"> URL Decode</span> "
		+ "<input type=\"checkbox\" name=\"auto_refresh\" onclick=\"window.auto_refresh=this.checked\" "+(gup("auto_refresh") ? "CHECKED" : "")+">"
		+ "<span style=\"font:11px arial,sans-serif;\"> Auto Refresh</span> "
		+ "<input type=\"checkbox\" name=\"s_object_names\" onclick=\"window.s_object_names=this.checked;request_list_run()\" "+(gup("s_object_names") ? "CHECKED" : "")+">"
		+ "<span style=\"font:11px arial,sans-serif;\"> Friendly Format</span>"
		+ "</td></tr>"
		+ "</table>"
		+ "<table border=\"0\" width=\"100%\">"
		+ "<tr><td id=\"request_list_cell\" align=\"center\" style=\"border: 0 none;\"></td></tr>"
		+ "</table>"
	);
	 
	// Footer
	document.write(''
		+ "<table border=\"0\" cellpadding=\"10\" width=\"100%\">"
		+ "<tr><td style=\"border: 0 none;align:center\"><br/><input type=\"button\" name=\"close2\" value=\"Close Window\" onclick=\"window.close();\"></td></tr></table>"
		+ "</form>"
		+ "</body></html>"
	);
	 

	 
	// Title, status, and background color
	document.title = 'Adobe Marketing Cloud Debugger';
	window.status = 'Adobe Marketing Cloud Debugger';

/*--Tbox Code--*/

var toolkit='<select id="select" name="select" style="margin:4px 5px 0 0;border:2px solid #000000;float:right;" onchange="runTnTools(this.options[this.options.selectedIndex].value)">';

toolkit+='<option value="-1" selected="true">[ Marketing Toolbox ]</option>'+
		'<option value="help">*What are these?</option>'+
		'<option value="atlist">atList</option>'+
		'<option value="mbox">mboxHighlight</option>'+
		'<option value="atdebug">VEC Offer Debugging</option>'+
		'<option value="profile">mboxProfile</option>'+
		'<option value="disable">mboxDisable</option>'+
//		'<option value="debug">mboxDebug</option>'+
//		'<option value="mcookie">mboxCookie</option>'+
//		'<option value="time">mboxTime</option>'+
//		'<option value="DOM">DOMconsole</option>'+
		'<option value="rerun">reseTnT</option>'+
//		'<option value="compare">compareTnT</option>'+
//		'<option value="onsite">OnSite</option>'+
//		'<option value="cookies">seeCookies</option>'+
		'<option value="delete">deleteCookies</option>'+
//		'<option value="ehtml">editHTML</option>'+
//		'<option value="ghtml">generatedHTML</option>'+
'</select>';

document.getElementById("box").innerHTML=toolkit;

function runTnTools(tool){
	switch(tool){
		case -1:return false; break;
		case "help":window.opener.document.body.innerHTML="<h1>What is the Adobe Target Toolbox?</h1></h2><p>The Adobe Target Toolbox is a collection of small JavaScript programs that when selected are simple debugging tools that when selected can add substantial functionality to the page you are viewing in your browser.	</p> <ul> <li><h2>atList</h2>atList uses data from the ttMETA response plugin (requires configuration) to quickly show you what Activities are running on which mboxes.  It will show multiple Activities delivered via a single mbox in both an alert window and in the console.</li><li><h2>mboxHighlight</h2>This tool highlights Test & Target mboxes on the page.  When selected, this tool shows you the Test & Target mboxes installed on the current page--whether default or imported--that you are viewing along with the mbox name all in bright \"red\".  This helps you research mbox names and locations when planning new campaigns or editing existing Test & Target content.  If the mbox title is \"mboxDefault\" you are viewing default content; if it shows \"mboxOffer\" then the mbox is loading a T&T offer.</li><li><h2>VEC Offer Debugging</h2><li>VEC Offer Debugging will turn on console statements related to Visual Experience Composer offer delivery.  It will reload your page with the query string ?_AT_Debug=console.</li><li><h2>mboxProfile</h2>This tool launching a popup which shows you the Test & Target profile data associated to you for your session in XML format.  An Authentication Token must be generated from the Target Classic Configuration->mbox.js->Edit screen and added to the page URL to use mboxProfile</li><li><h2>mboxDisable</h2>This tool toggles Test & Target \"ON\" or \"OFF\".  In the \"OFF\" mode all campaign offers and dynamic content are disabled so you see only the default content.  To turn Test & Target back on, simply select the tool again.</li><li><h2>reseTnT</h2>Tired of having to delete your cookies, clear your cache, and open a new browser to view a different mbox campaign recipe?  Simply select this tool and it deletes all your mbox 1st party session cookies and reloads the page for you allowing you to be treated as a \"first time\" guest and possibly recieving different Test & Target campaign data--provided that you qualify for the campaigns targeting rules that have been set up.</li><li><h2>deleteCookies</h2>This tool will ask you to confirm and then delete all the cookies that you have set at that website domain.  This same functionality can be found in your browser at \"Tools>Internet Options>Delete Cookies\" for Internet Explorer, \"Tools>Options>Cookies>Clear Cookies Now\" for Firefox. Please Note: If you are logged in and you delete your cookies, you will have to log in again.</li> </ul>";break;
		/* <li><h2>mboxDebug</h2>This tool turns on the Test & Target \"Debug Mode\", launching a popup which shows you the mbox campaign data returned from Test & Target for the current page you are viewing.  To exit the debug mode, simply select the tool again.</li> 	<li><h2>mboxCookie</h2>This tool turns on the Test & Target \"Debug Mode\", launching a popup which shows you the mbox cookie values that you have on the domain you are viewing.  You can edit, delete or even regenerate your mbox cookie data.  To exit the debug mode, simply select the tool again.</li><li><h2>compareTnT</h2>This tool splits your current page into 2 frames that load the page you are currently on side-by-side.  The page in the left frame loads TnT dynamic campaign content while the frame on the right shows you that same page's default content.</li>  <li><h2>OnSite</h2>This selection launches the Test & Target OnSite mode.</li> <li><h2>seeCookies</h2>This tool launches an alert box that displays all the cookies you have set at that current website domain.</li> <li><h2>editHTML</h2>This tool launches a popup that displays the generated source for the current page.  You can make changes and then click the \"Update\" button to update the original page.</li> <li><h2>generatedHTML</h2>This tool translates the page you are currently viewing into it's generated HTML source which is very useful for debugging javascript issues.</li>*/
		//case "m":window.opener.location="javascript:var%20M=0,H=[],I='',L=document.getElementsByTagName('div');for(i=0;i<L.length;i++){if(L[i].className.match('oxDef')||L[i].id.match('xImp')){M=1;L[i].style.border='2px%20solid%20red';id=L[i].id;H[i]=L[i].innerHTML;if(id==''){if(L[i].nextSibling){n=L[i].nextSibling;I=n.innerHTML;if(!I)I=n.nextSibling.innerHTML}try{id=I.match(/eate\((.*)\)/)[1]}catch(e){id='\?'}}t='mbox:%20'+id;try{L[i].innerHTML='<h2%20onclick=alert(H['+i+'])%20style=color:red>'+t+'</h2>'+H[i]}catch(e){}}}if(M<1){alert('None')}void(0)";break;
//		case "mbox":window.opener.location="javascript:try{if(typeof(mboxVersion)=='number'){mboxFactoryDefault.getMboxes().each(function(x){var%20m=x.getDiv(),I=m.innerHTML,T=(!m.id.match('Impo'))?'mbox':'mbox-Offer',D=(T=='mbox'),O='-Default',U=unescape(x.getURL());m.style.border='2px%20solid%20red';if(!D){O='-'+I.split('fer%20Id:%20')[1].split('--')[0]}m.innerHTML='<h2%20title=\"'+x.getImportName()+'\r|\r'+U+'\"style=\"color:red\"onclick=open(\"'+U+'\",\"\",\"\")>'+T+O+':\"'+x.getName()+'\"</h2>'+I})}else{alert('None')}}catch(e){dpDB.runTnTools('m')}";break;
		case "mbox":window.opener.location="javascript:var%20j=0,H=[];try{mboxFactoryDefault.getMboxes().each(function(x){var%20t='',mt='',nmt='',ttl='';n=x.getName(),m=x.getDiv(),I=m.innerHTML,T=(!m.id.match('Impo'))?'mbox':'mbox-Offer',D=(T=='mbox'),E=typeof(window.ttMBX)!='undefined',O='default',U=decodeURI(x.getURL()),c=document.createElement('code');c.innerHTML=I.split('&').join('&amp;').split('<').join('&lt;').split('>').join('&gt;').split('%22').join('&quot;').split('%20').join('&nbsp;').split('&lt;!--').join('<br><br>&lt;!--').split('--&gt;').join('--&gt;<br>');c.style.border='1px%20solid%20red';c.style.display='inline-block';c.style.cursor='auto';H[j]=c;m.style.border='2px%20solid%20red';m.style.boxShadow='10px%2010px%205px%20#888';if(!D){O=I.split('fer%20Id:%20')[1].split('--')[0]}if(E){M=ttMBX(x);(!M)?mt+='mbox:%20'+n+'<br>(check if mbox is deactivated and if mboxHighlight plugin is served to %22All Mbox Requests%22)':'';for(i%20in%20M){mt+=i+':%20'+eval('M.'+i)+'';mt+='<br>'}};nmt+='mbox:%20'+n+'<br>offer:%20'+O;%20if(E){(mt=='deact')?t+=nmt+'<br><i>'+mt+'</i>':t=mt}else{t=nmt};m.innerHTML='<h2%20'+ttl+'style=%22position:relative;z-index:1000;cursor:pointer;color:red!important;z-index:9999%22onclick=this.appendChild(H['+j+'])>'+t+'</h2>'+I;if((m.parentNode)&&m.parentNode.style.display==%22none%22){m.parentNode.style.display=%22block%22};j++})}catch(e){alert('None')}";break;
		case "disable":window.opener.location="javascript:dl=location;q=dl.search;if(q.indexOf('mboxDisable')==-1){y=(q=='')?'?':'&';y+='mboxDisable=1';location=dl.toString()+y}else{location=dl.toString().replace(/[?|&]?mboxDisable=1/,'')}void(0)";break;
		//case "debug":window.opener.location="javascript:dc=document.cookie;dl=location;q=dl.search;if(q.indexOf('mboxDebug')==-1&&dc.indexOf('debug#')==-1){y=(q=='')?'?':'&';y+='mboxDebug=1';location=dl.toString()+y}else{mboxFactoryDefault.getCookieManager().deleteCookie('debug');location=dl.toString().replace(/[?|&]?mboxDebug=.*/,'')}void(0)";break;
		//case "mcookie":window.opener.location="javascript:dc=document.cookie;dl=location;q=dl.search;if(q.indexOf('mboxDebug')==-1&&dc.indexOf('debug#')==-1){y=(q=='')?'?':'&';y+='mboxDebug=x-cookie';location=dl.toString()+y}else{mboxFactoryDefault.getCookieManager().deleteCookie('debug');location=dl.toString().replace(/[?|&]?mboxDebug=.*/,'')}void(0)";break;
//		case "profile":window.opener.location="javascript:dc=document.cookie;dl=location;q=dl.search;if(q.indexOf('mboxDebug')==-1&&dc.indexOf('debug#')==-1){y=(q=='')?'?':'&';y+='mboxDebug=x-profile';location=dl.toString()+y}else{mboxFactoryDefault.getCookieManager().deleteCookie('debug');location=dl.toString().replace(/[?|&]?mboxDebug=.*/,'')}void(0)";break;
		case "profile":window.opener.location="javascript:var u,w,v='',m=mboxFactoryDefault;if(navigator.userAgent.toLowerCase().indexOf('chrome')>-1){v='view-source:'};if(m.getMboxes().length()>0){m=m.getMboxes().getById(0);u=m.getURL().split('.tt.')[0].split('//')[1];w=v+'http://'+u+'.tt.omtrdc.net/debug/x-profile.jsp?clientCode='+u+'&mboxSession='+mboxFactoryDefault.getSessionId().getId()+'&mboxPC='+mboxFactoryDefault.getPCId().getId()+'&r='+Math.random();open(w,w,'scrollbars=yes,resizable=yes')}else{alert('No mboxes found on the page')}void(0);";break;
//		case "time":window.opener.location="javascript:dc=document.cookie;dl=location;q=dl.search;if(q.indexOf('mboxDebug')==-1&&dc.indexOf('debug#')==-1){y=(q=='')?'?':'&';y+='mboxDebug=x-time';location=dl.toString()+y}else{mboxFactoryDefault.getCookieManager().deleteCookie('debug');location=dl.toString().replace(/[?|&]?mboxDebug=.*/,'')}void(0)";break;
		//case "onsite":window.opener.location="javascript:(function()%20{var%20mboxBookmarklet%20=%20document.createElement('script');%20mboxBookmarklet.src='https://testandtarget.omniture.com/user/bookmarklet.jsp';%20document.body.appendChild(mboxBookmarklet);%20})();";break;
//		case "DOM":window.opener.location="javascript:(function(){var%20s='http://chriszak.com/TnTools/DOMconsole.js',%20t='text/javascript',q='\u0022',d=document,n=navigator,e;if(/mac/i.test(n.platform)&&/msie/i.test(n.userAgent))(d.createElement('div')).innerHTML='\u003cscript%20type='+q+t+q+'%20src='+q+s+q+'\u003e\u003c/script\u003e';else{(e=d.createElement('script')).src=s;e.type=t;d.getElementsByTagName('head')[0].appendChild(e)}})();void(0)";break;
		case "compare":window.opener.location="javascript:var L0c=location.href,Fr4,mD1s=(L0c.indexOf('?')!=-1)?'&':'?';mD1s+='mboxDisable=1';Fr4='<frameset%20cols=\"*,*\">\n<frame%20src=\"'+L0c+'\"/><frame%20src=\"'+L0c+mD1s+'\"/>\n</frameset>';document.getElementsByTagName('html')[0].innerHTML=Fr4;";break;		
		case "rerun":window.opener.location="javascript:var dc=document.cookie,mc='PC,geo,session,check,debug,edge',no='';mc=mc.split(',');for(i=0;i<mc.length;i++){try{mboxFactoryDefault.getCookieManager().deleteCookie(mc[i])}catch(e){no+=mc[i]+' \n'}}if(no!=''){alert('No '+no+'mbox Cookies Found!')}location.reload();void(0);";break;
		case "cookies":window.opener.location="javascript:void(alert(unescape(document.cookie.split(';%20').join('\n\n'))));";break;
		case "delete":window.opener.location="javascript:(function(){C=document.cookie.split(';%20');for(d='.'+location.host;d;d=(''+d).substr(1).match(/\..*$/))for(sl=0;sl<2;++sl)for(p='/'+location.pathname;p;p=p.substring(0,p.lastIndexOf('/')))for(i%20in%20C)if(c=C[i]){document.cookie=c+';%20domain='+d.slice(sl)+';%20path='+p.slice(1)+'/'+';%20expires='+new%20Date((new%20Date).getTime()-1e11).toGMTString()}})();window.location.reload();";break;
		case "ehtml":window.opener.location="javascript:var db=document.getElementsByTagName('html')[0],biH=db.innerHTML;biH='<html><body><script>window.onresize=function(){document.f.t.style.height=document.body.clientHeight-60}</script><form name=f><input type=\"button\"value=\"UPDATE\"onclick=opener.document.getElementsByTagName(\"body\")[0].innerHTML=document.f.t.value;><textarea name=\"t\"rows=\"21\"style=\"width:100%\">'+biH+'</textarea>';with(open('','','width=900,height=400,scrollbars=yes,resizable=yes').document){write(biH);close()}";break;
		case "ghtml":window.opener.location="javascript:var db=document.getElementsByTagName('html')[0],biH=db.innerHTML;biH=biH.replace(/\</g,'<').replace(/\\n/g,'<br>');";break;
		
		case "atlist":window.opener.location="javascript:javascript:var showConsole%3Dtrue,showAlert%3Dtrue,msg%3D\"\"%3Bif(\"undefined\"!%3D%3Dtypeof ttMETA)%7Bfor(var key in ttMETA)if(ttMETA.hasOwnProperty(key))%7Bvar obj%3DttMETA%5Bkey%5D,prop%3Bfor(prop%20in%20obj)obj.hasOwnProperty(prop)%26%26(msg%2B%3Dprop%2B\":\"%2Bobj%5Bprop%5D%2B\"%5Cn\")%3Bmsg%2B%3D\"-------------------------------%5Cn\"%7DshowConsole%26%26console%26%26console.log%26%26(\"function\"%3D%3D%3Dtypeof%20console.table%3Fconsole.table(ttMETA):console.log(msg))%3BshowAlert%26%26alert(msg)%7Delse showConsole%26%26console%26%26console.log%26%26console.log(\"ttMETA object does not exist\"),showAlert%26%26alert(\"ttMETA object does not exist\")%3B";break;
		
		case "atdebug":window.opener.location="javascript:dl=location;q=dl.search;if(q.indexOf('_AT_Debug')==-1){y=(q=='')?'?':'&';y+='_AT_Debug=console';location=dl.toString()+y}else{location=dl.toString().replace(/[?|&]?_AT_Debug=console/,'')}void(0)";break;

		default:return false;
	}
	document.getElementById("select").options[0].selected="true";
}
	 
	// Usage tracking
	var account_set = ''; 
document.write('<im'+'g sr'+
//'c="https://omnitureengineering.d1.sc.omtrdc.net/b/ss/omniture.dp.debugger.dev/1/TAG/s0.317827001254522398/?pageName=DigitalPulse%20Debugger_v15.12&c1='+window.location.host+'" width="0" height="0" border="0" alt="0">');
'c="https://omnitureengineering.d1.sc.omtrdc.net/b/ss/omniture.dp.debugger/1/TAG/s0.317827001254522398/?pageName=DigitalPulse%20Debugger_v15.12&c1='+window.location.host+'" width="0" height="0" border="0" alt="0">');

//document.close();
 
// Get and display requests
window.url_decode = gup("url_decode"); 
window.auto_refresh = gup("auto_refresh");
window.s_object_names = gup("s_object_names");
window.tnt = gup("tnt");
window.recs = gup("recs");
window.sc = gup("sc");
window.am = gup("am");
window.survey = gup("survey"); //alert(window.survey);
window.amo = gup("amo");
request_list_run();